# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mkinitfs
pkgver=3.6.0_rc2
# shellcheck disable=SC2034 # used for git versions, keep around for next time
_ver=${pkgver%_git*}
pkgrel=1
pkgdesc="Tool to generate initramfs images for Alpine"
url="https://gitlab.alpinelinux.org/alpine/mkinitfs"
arch="all"
license="GPL-2.0-only"
# currently we do not ship any testsuite
options="!check"
makedepends_host="busybox kmod-dev util-linux-dev cryptsetup-dev linux-headers"
makedepends="$makedepends_host"
depends="busybox>=1.28.2-r1 apk-tools>=2.9.1 lddtree>=1.25 kmod"
subpackages="$pkgname-doc"
install="$pkgname.pre-upgrade $pkgname.post-install $pkgname.post-upgrade"
triggers="$pkgname.trigger=/usr/share/kernel/*"
source="https://gitlab.alpinelinux.org/alpine/mkinitfs/-/archive/$pkgver/mkinitfs-$pkgver.tar.gz
	0001-nlplug-findfs-fix-search-device.patch
	"

build() {
	make VERSION=$pkgver-r$pkgrel
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
6c1f7ee1351b601dda90fb3a8eee32e32363a0deb35e79df6488b6e670c254bd9a719f15ad01dd5bc18981ebf418cfd56f1da82506fab77775c7c37e2d881f44  mkinitfs-3.6.0_rc2.tar.gz
31b855223fa37cdbabb0c68a8c439c2e4fea603786dc8207ac72e26c6f74e6f1d69e0c1e7b456037e7bf3d12f73a9bc021719e4d310a45f7f45528db7f0f4395  0001-nlplug-findfs-fix-search-device.patch
"
